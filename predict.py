# --------------------------------------------------------------------------
# ------------  Metody Systemowe i Decyzyjne w Informatyce  ----------------
# --------------------------------------------------------------------------
#  Zadanie 4: Zadanie zaliczeniowe
#  autorzy: A. Gonczarek, J. Kaczmar, S. Zareba
#  2017
# --------------------------------------------------------------------------

import pickle as pkl
import time
import numpy as np
import random
import sys

PICKLE_FILE_PATH = 'train.pkl'
PICKLE_VALUES_FILE_PATH = "C:\\Users\\filip\\PycharmProjects\\zadanie4LogRegPred\\values.pkl"


def load_values():
    with open(PICKLE_VALUES_FILE_PATH, 'rb') as f:
        return pkl.load(f)


def load_data():
    with open(PICKLE_FILE_PATH, 'rb') as f:
        return pkl.load(f)


def softmax(x):
    exp_x = np.exp(x)
    return exp_x/np.sum(exp_x,axis=1,keepdims=True)


def stochastic_gradient_descent(obj_fun, x_train, y_train, w0, epochs, eta, mini_batch, lambd):
    w = w0
    M = int(x_train.shape[0]/mini_batch)
    x_data = zip(x_train, y_train)
    for i in range(epochs):
        x_data=list(x_data)
        random.shuffle(x_data)
        x_train1, y_train1 = zip(*x_data)
        x_train1 = np.asarray(x_train1)
        y_train1=np.asarray(y_train1)
        iterator = 0
        for m in range(M):
            w += (obj_fun(w,x_train1[iterator:iterator+mini_batch],y_train1[iterator:iterator+mini_batch],lambd))*eta
            iterator += mini_batch
        x_data = zip(x_train1, y_train1)
    return w


def prediction(x, w):
    return np.argmax(softmax(x@w.transpose()),axis=1)


def feature_extract(x):
    result = np.zeros(shape=(x.shape[0],175))
    for i in range(x.shape[0]):
        result[i]=hog(x[i])
    return result


def hog(image):
    image = np.reshape(image,(56,56))
    nwin_x = 5
    nwin_y = 5
    B = 7
    (L, C) = np.shape(image)
    H = np.zeros(shape=(nwin_x * nwin_y * B, 1))
    if C is 1:
        raise NotImplementedError
    step_x = np.floor(C / (nwin_x + 1))
    step_y = np.floor(L / (nwin_y + 1))
    cont = 0
    hx = [1, 0, -1]
    hy = [-1, 0, 1]
    grad_xr = np.zeros([L, C])
    for i in range(L):
        grad_xr[i, :] = np.convolve(image[i, :], hx, "same")

    grad_yu = np.zeros([L, C])
    for i in range(C):
        grad_yu[:, i] = np.convolve(image[:, i], hy, "same")

    angles = np.arctan2(grad_yu, grad_xr)
    magnit = np.sqrt((grad_yu ** 2 + grad_xr ** 2))
    for n in range(nwin_y):
        for m in range(nwin_x):
            cont += 1
            angles2 = angles[int(n * step_y):int((n + 2) * step_y),
                      int(m * step_x):int((m + 2) * step_x)]
            magnit2 = magnit[int(n * step_y):int((n + 2) * step_y),
                      int(m * step_x):int((m + 2) * step_x)]
            v_angles = angles2.ravel()
            v_magnit = magnit2.ravel()
            bin = 0
            H2 = np.zeros(shape=(B, 1))

            for ang_lim in np.arange(start=-np.pi + 2 * np.pi / B,
                                     stop=np.pi + 2 * np.pi / B,
                                     step=2 * np.pi / B):
                check = v_angles < ang_lim
                v_angles = (v_angles * (~check)) + (check) * 100
                H2[bin] += np.sum(v_magnit * check)
                bin += 1

            H2 = H2 / (np.linalg.norm(H2) + 0.01)
            H[(cont - 1) * B:cont * B] = H2
    return np.reshape(H,175)


def logistic_cost_function(w, x_train, y_train, lambd):
    wWithoutZero = np.array(w)
    wWithoutZero[:,0]=0
    return (y_train-softmax(x_train@w.transpose())).transpose()@x_train + lambd*wWithoutZero


def measure(y_true, y_pred):
    return np.sum(y_true==y_pred)


def model_selection(x_train, y_train, x_val, y_val, w0, epochs, mini_batches, lambdas,etas):
    vals = np.zeros((len(lambdas),len(epochs),len(etas),len(mini_batches)))
    highest=0
    bestLambda = lambdas[0]
    bestEpochs = epochs[0]
    bestEta = etas[0]
    bestMiniBatch = mini_batches[0]
    for i in range(len(lambdas)):
        for k in range(len(etas)):
            for l in range(len(mini_batches)):
                wx=stochastic_gradient_descent(logistic_cost_function,x_train,y_train,np.array(w0),epochs[0],etas[k],mini_batches[l],lambdas[i])
                y_pred1 = prediction(x_val,wx)
                vals[i,0,k,l]=measure(y_val, np.reshape(y_pred1,(y_pred1.shape[0],1,1)))
                if highest < vals[i, 0, k, l]:
                    bestLambda = lambdas[i]
                    bestEta = etas[k]
                    bestMiniBatch = mini_batches[l]
                    bestEpochs = epochs[0]
                    highest = vals[i,0, k, l]
                for j in range(1,len(epochs)):
                    stochastic_gradient_descent(logistic_cost_function, x_train, y_train, wx, 1, etas[k],mini_batches[l], lambdas[i])
                    y_pred = prediction(x_val, wx)
                    vals[i,j,k,l] = measure(y_val, np.reshape(y_pred,(y_pred.shape[0],1,1)))
                    if highest<vals[i,j,k,l]:
                        bestLambda = lambdas[i]
                        bestEta = etas[k]
                        bestMiniBatch = mini_batches[l]
                        bestEpochs = epochs[j]
                        highest = vals[i, j, k, l]
            print(k)
        print(i)

    print(highest/x_val.shape[0])
    return bestLambda,bestEpochs,bestEta,bestMiniBatch,vals


if __name__ == '__main__':
    data = load_data()
    number_of_classes = 36
    X = data[0]
    Y = data[1]
    x_train = X[:21000]
    x_train_extracted = feature_extract(x_train)
    x_val = X[21000:]
    x_val_extracted = feature_extract(x_val)

    y_train = Y[:21000]
    y_val = Y[21000:]
    y_trainOneHot = np.zeros(shape=(y_train.shape[0], number_of_classes))
    for i in range(y_train.shape[0]):
        y_trainOneHot[i, y_train[i]] = 1

    epochs=[1]
    for i in range(2,1000):
        epochs.append(i)
    lambdas=[0,0.0001]
    mini_batches=[50,100]
    w_0 = np.zeros([number_of_classes,175])
    etas=[0.001,0.002,0.003,0.004,0.005,0.006,0.007,0.008,0.009]
    bLambds,bEpochs,bEta,bMiniBatch,vals = model_selection(x_train_extracted,y_trainOneHot,x_val_extracted,y_val,w_0,epochs,mini_batches,lambdas,etas)
    print(bMiniBatch)
    print(bLambds)
    print(bEpochs)
    print(bEta)
    print(vals)
    xfull = X
    yfull=Y
    xfullex = feature_extract(xfull)
    yfulloneHot = np.zeros(shape=(yfull.shape[0], number_of_classes))
    for i in range(yfull.shape[0]):
        yfulloneHot[i, yfull[i]] = 1
    w = stochastic_gradient_descent(logistic_cost_function,xfullex,yfulloneHot,np.array(w_0),bEpochs,bEta,bMiniBatch,bLambds)
    values = [w, bEpochs, bEta,bMiniBatch,bLambds]

    file_Name = 'values.pkl'
    fileObject = open(file_Name, 'wb')
    pkl.dump(values, fileObject)
    fileObject.close()


